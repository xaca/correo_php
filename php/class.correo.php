<?php
require_once('../phpmailer/class.phpmailer.php');
require_once('../phpmailer/class.smtp.php');

class Correo { 
        // holds the combinations 
    private $correoEmisor;                
    private $correoReceptor;
    private $nombreEmisor;
    private $asunto;
    private $mensajeHTML;
    private $mensajePlano;
    
    public function correoEmisor($correoEmisor){
    	$this->correoEmisor = htmlspecialchars($correoEmisor);
    }
       
    public function correoReceptor($correoReceptor){
    	$this->correoReceptor = htmlspecialchars($correoReceptor);
    }

    public function nombreEmisor($nombreEmisor){
    	$this->nombreEmisor = utf8_decode($nombreEmisor);
    }

    public function asunto($asunto){
    	$this->asunto = utf8_decode($asunto);
    }

    public function mensajeHTML($mensajeHTML){
    	$this->mensajeHTML = utf8_decode($mensajeHTML);
    }

    public function mensajePlano($mensajePlano){
    	$this->mensajePlano = utf8_decode($mensajePlano);
    }

	public function enviarEmail() {
   
		//$asunto = '?UTF-8?B?'.base64_encode("Tu contraseña ha sido restablecida").'?=';
		//$asunto = utf8_decode($obj->asunto);
		$mail = new PHPMailer();
		$mail->SetFrom($this->correoEmisor);
		$mail->FromName = $this->nombreEmisor;
		$mail->Subject = $this->asunto;
		$mail->IsHTML(true);
		$mail->MsgHTML($this->mensajeHTML);
		//    $mail->Body = $texto;
		$mail->AddAddress($this->correoReceptor);

		if (!$mail->Send()) 
		{
		    return array('op' => true, "msg" => ($mail->ErrorInfo)." ".$this->correoEmisor);
		} 
		else 
		{
		    return array('op' => false, 'email' => " Correo enviado correctamente ".$this->correoReceptor);
		}
	}
}
?>       