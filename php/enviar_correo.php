<?php
require_once('class.correo.php');

if (isset($_REQUEST['usuario']) && isset($_REQUEST['correo'])) {
    $enlace_temporal = "";//Acá va el enlace para cambiar el correo con fecha de expiración
    $receptor = $_REQUEST['correo']; //Acá va el correo del usuario
    $obj = new Correo();
    $obj->correoEmisor("no_reply@sucorreo.com");
    $obj->correoReceptor($receptor);
    $obj->nombreEmisor('Su nombre acá');
    $obj->asunto("Petición recordar contraseña");
    // Retrieve the email template required 
    $message = file_get_contents("../correo/plantilla.html"); 
    // Replace the % with the actual information 
    $message = str_replace('%usuario%', $_REQUEST['usuario'], $message); 
    $message = str_replace('%enlace%', $enlace_temporal, $message); 	
    //$obj->mensajeHTML('<table width="700" height="850" align="center" cellpadding="0" cellspacing="0"><tr><td><img src="http://agencia.pragma.com.co/auteco/re/img/mail_estacas_color_final.jpg" border="0" usemap="#Map" /></td></tr></table><map name="Map" id="Map"><area shape="rect" coords="3,708,604,853" href="http://agencia.pragma.com.co/auteco/re/" /><area shape="rect" coords="605,707,704,849" href="http://agencia.pragma.com.co/auteco/re/" /></map>');
    $obj->mensajeHTML($message);                    
    echo json_encode($obj->enviarEmail());
}
else{
    echo json_encode(array('op' => true, "msg" => "Falta el email"));
}   

?>