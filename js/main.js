var botonEnviar;
var formulario;
var url_script;

function init () {
	url_script = "php/enviar_correo.php";
	botonEnviar = $("#botonEnviar");
	formulario = $("#recordarPassword");
	botonEnviar.click(validarDatos);
}
function validarDatos(e) {
	var usuario = $("#usuario");
	var correo = $("#correo");
	if(usuario.val() != "" && correo.val() != ""){
		enviarDatos();
	}
	else
	{
		alert("Error verifica los datos");
	}
}
function enviarDatos(e){
	$.ajax({ 
	   method:"post",
       url: url_script,
       data: formulario.serialize()+"&rnd="+Math.random(),
       dataType: "json",
       success: function(data) {              
          console.log(data);                         
      }  
   });
}
window.onload = init;